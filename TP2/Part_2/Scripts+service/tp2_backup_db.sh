#!/bin/bash
#Script de backup de database
#theo - 24/10/2021

DATE=$(date +"%m-%d-%Y")
HOUR=$(date +"%H-%M-%S")
mysqldump -u root -p nextcloud > nextcloud.sql
tar -zcvf tp2_backup_$DATE-$HOUR.tar.gz $2
rsync tp2_backup_$DATE-$HOUR.tar.gz $1
cp nextcloud.sql $1
