#!/bin/bash
# Configure fail2ban for sshd
# theo ~ 14/11/2021

touch /etc/fail2ban/jail.d/sshd.local
echo '[sshd]' > /etc/fail2ban/jail.d/sshd.local
echo 'enabled = true' >> /etc/fail2ban/jail.d/sshd.local
echo 'bantime = 1d' >> /etc/fail2ban/jail.d/sshd.local
echo 'maxretry = 3' >> /etc/fail2ban/jail.d/sshd.local
systemctl restart fail2ban.service
fail2ban-client status
