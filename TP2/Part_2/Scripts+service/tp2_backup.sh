#!/bin/bash
#Script de backup
#theo - 12/10/2021

DATE=$(date +"%m-%d-%Y")
HOUR=$(date +"%H-%M-%S")
tar -zcvf tp2_backup_$DATE-$HOUR.tar.gz $2
rsync tp2_backup_$DATE-$HOUR.tar.gz $1
