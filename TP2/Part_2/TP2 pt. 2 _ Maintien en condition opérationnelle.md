QUESNOY Théo

# TP2 pt. 2 : Maintien en condition opérationnelle

# Sommaire

- [TP2 pt. 2 : Maintien en condition opérationnelle](#tp2-pt-2--maintien-en-condition-opérationnelle)
- [Sommaire](#sommaire)
- [I. Monitoring](#i-monitoring)
  - [2. Setup](#2-setup)
- [II. Backup](#ii-backup)
  - [2. Partage NFS](#2-partage-nfs)
  - [3. Backup de fichiers](#3-backup-de-fichiers)
  - [4. Unité de service](#4-unité-de-service)
    - [A. Unité de service](#a-unité-de-service)
    - [B. Timer](#b-timer)
    - [C. Contexte](#c-contexte)
  - [5. Backup de base de données](#5-backup-de-base-de-données)
- [III. Reverse Proxy](#iii-reverse-proxy)
  - [2. Setup simple](#2-setup-simple)
- [IV. Firewalling](#iv-firewalling)
  - [2. Mise en place](#2-mise-en-place)
    - [A. Base de données](#a-base-de-données)
    - [B. Serveur Web](#b-serveur-web)
    - [C. Serveur de backup](#c-serveur-de-backup)
    - [D. Reverse Proxy](#d-reverse-proxy)
    - [E. Tableau récap](#e-tableau-récap)

# I. Monitoring

## 2. Setup

* Installation Netdata
```
[theo@web ~]$ sudo su -
[root@web ~]# bash <(curl -Ss https://my-netdata.io/kickstart-static64.sh)
 --- Downloading static netdata binary: https://storage.googleapis.com/netdata-nightlies/netdata-latest.gz.run ---
[/tmp/netdata-kickstart-ahnGUO3J2Q]# curl -q -sSL --connect-timeout 10 --retry 3 --output /tmp/netdata-kickstart-ahnGUO3J2Q/sha256sum.txt https://storage.googleapis.com/netdata-nightlies/sha256sums.txt
[...]
[root@web ~]# exit
logout
```

* Vérification 

```
[theo@web ~]$ systemctl status netdata
● netdata.service - Real time performance monitoring
   Loaded: loaded (/usr/lib/systemd/system/netdata.service; enabled; vendor preset: disabled)
   Active: active (running) since Mon 2021-10-11 14:51:31 CEST; 1min 31s ago
[...]
[theo@web ~]$ systemctl is-enabled netdata
enabled
```

* Port netdata

```
[theo@web ~]$ sudo ss -alnpt | grep 'netdata'
LISTEN 0      128        127.0.0.1:8125       0.0.0.0:*    users:(("netdata",pid=2202,fd=44))                           
LISTEN 0      128          0.0.0.0:19999      0.0.0.0:*    users:(("netdata",pid=2202,fd=5))                            
LISTEN 0      128            [::1]:8125          [::]:*    users:(("netdata",pid=2202,fd=43))                           
LISTEN 0      128             [::]:19999         [::]:*    users:(("netdata",pid=2202,fd=6))
```
* Autorisation sur le firewall

```
[theo@web ~]$ sudo firewall-cmd --add-port=19999/tcp
success
[theo@web ~]$ sudo firewall-cmd --add-port=19999/tcp --permanent
success
[theo@web ~]$ sudo firewall-cmd --list-all | grep 'ports'
  ports: 80/tcp 19999/tcp
```

* Alertes test discord 

```
[theo@web netdata]$ sudo /opt/netdata/usr/libexec/netdata/plugins.d/alarm-notify.sh test

# SENDING TEST WARNING ALARM TO ROLE: sysadmin
grep sysadmin /opt/netdata/etc/netdata/health_alarm_notify.conf
role_recipients_email[sysadmin]="${DEFAULT_RECIPIENT_EMAIL}"
role_recipients_pushover[sysadmin]="${DEFAULT_RECIPIENT_PUSHOVER}"
role_recipients_pushbullet[sysadmin]="${DEFAULT_RECIPIENT_PUSHBULLET}"
role_recipients_telegram[sysadmin]="${DEFAULT_RECIPIENT_TELEGRAM}"
role_recipients_slack[sysadmin]="${DEFAULT_RECIPIENT_SLACK}"
2021-10-11 16:50:32: alarm-notify.sh: INFO: sent discord notification for: web.tp2.linux test.chart.test_alarm is WARNING to 'alarms'
# OK

# SENDING TEST CRITICAL ALARM TO ROLE: sysadmin
grep sysadmin /opt/netdata/etc/netdata/health_alarm_notify.conf
role_recipients_email[sysadmin]="${DEFAULT_RECIPIENT_EMAIL}"
role_recipients_pushover[sysadmin]="${DEFAULT_RECIPIENT_PUSHOVER}"
role_recipients_pushbullet[sysadmin]="${DEFAULT_RECIPIENT_PUSHBULLET}"
role_recipients_telegram[sysadmin]="${DEFAULT_RECIPIENT_TELEGRAM}"
role_recipients_slack[sysadmin]="${DEFAULT_RECIPIENT_SLACK}"
2021-10-11 16:50:33: alarm-notify.sh: INFO: sent discord notification for: web.tp2.linux test.chart.test_alarm is CRITICAL to 'alarms'
# OK

# SENDING TEST CLEAR ALARM TO ROLE: sysadmin
grep sysadmin /opt/netdata/etc/netdata/health_alarm_notify.conf
role_recipients_email[sysadmin]="${DEFAULT_RECIPIENT_EMAIL}"
role_recipients_pushover[sysadmin]="${DEFAULT_RECIPIENT_PUSHOVER}"
role_recipients_pushbullet[sysadmin]="${DEFAULT_RECIPIENT_PUSHBULLET}"
role_recipients_telegram[sysadmin]="${DEFAULT_RECIPIENT_TELEGRAM}"
role_recipients_slack[sysadmin]="${DEFAULT_RECIPIENT_SLACK}"
2021-10-11 16:50:33: alarm-notify.sh: INFO: sent discord notification for: web.tp2.linux test.chart.test_alarm is CLEAR to 'alarms'
# OK
```

* Alertes RAM

```
[theo@web ~]$ sudo cat /opt/netdata/etc/netdata/health.d/ram-usage.conf
alarm: ram_usage
 on: system.ram
lookup: average -1m percentage of used
 units: %
 every: 1m
 warn: $this > 50
 crit: $this > 80
 info: The percentage of RAM used by the system.
```

# II. Backup

## 2. Partage NFS

* Création dossier NFS sur backup

```
[theo@backup ~]$ sudo mkdir /srv/backup/web.tp2.linux
[theo@backup ~]$ ls /srv/backup/
web.tp2.linux
```

* NFS côté web

```
[theo@web ~]$ sudo mount -t nfs backup.tp2.linux:/srv/backup/web.tp2.linux /srv/backup
[theo@web ~]$ mount | grep 'backup'
backup.tp2.linux:/srv/backup/web.tp2.linux on /srv/backup type nfs4 (rw,relatime,vers=4.2,rsize=131072,wsize=131072,namlen=255,hard,proto=tcp,timeo=600,retrans=2,sec=sys,clientaddr=10.102.1.11,local_lock=none,addr=10.102.1.13)
[theo@web ~]$ df -hT | grep 'backup'
backup.tp2.linux:/srv/backup/web.tp2.linux nfs4      6.2G  2.1G  4.2G  34% /srv/backup
[theo@web backup]$ sudo touch test_backup
[theo@web srv]$ cat /etc/fstab | grep 'backup'
backup.tp2.linux:/srv/backup/web.tp2.linux /srv/backup nfs defaults     0 0
```

* Vérification sur backup 

```
[theo@backup ~]$ ls /srv/backup/web.tp2.linux/
test_backup
```

## 3. Backup de fichiers

* Test du script de backup

```
[theo@web srv]$ sudo ./tp2_backup.sh /srv/backup/ /home/archive_script
tar: Removing leading `/' from member names
/home/archive_script
[theo@web srv]$ ls backup/
test_backup  tp2_backup_10-12-2021-16-03-30.tar.gz
[theo@web backup]$ sudo tar -tf tp2_backup_10-12-2021-16-03-30.tar.gz
home/archive_script
[theo@web backup]$ sudo tar --extract --file=tp2_backup_10-12-2021-16-03-30.tar.gz
[theo@web backup]$ ls
home  test_backup  tp2_backup_10-12-2021-16-03-30.tar.gz
```

Fichier [tp2_backup.sh](./Scripts+service/tp2_backup.sh)

## 4. Unité de service

### A. Unité de service

* Création de service de backup 

```
[theo@web backup]$ sudo vim /etc/systemd/system/tp2_backup.service
[theo@web backup]$ sudo systemctl daemon-reload
[theo@web backup]$ sudo systemctl start tp2_backup
[theo@web backup]$ ls
home  test_backup  tp2_backup_10-12-2021-16-03-30.tar.gz  tp2_backup_10-12-2021-16-48-30.tar.gz
```

### B. Timer

```
[theo@web backup]$ sudo vim /etc/systemd/system/tp2_backup.timer
[theo@web backup]$ sudo systemctl start tp2_backup.timer
[theo@web backup]$ sudo systemctl enable tp2_backup.timer
Created symlink /etc/systemd/system/timers.target.wants/tp2_backup.timer → /etc/systemd/system/tp2_backup.timer.
```

* Vérification 

```
[theo@web backup]$ systemctl status tp2_backup.timer
● tp2_backup.timer - Periodically run our TP2 backup script
   Loaded: loaded (/etc/systemd/system/tp2_backup.timer; enabled; vendor preset: disabled)
   Active: active (waiting) since Tue 2021-10-12 16:52:17 CEST; 52s ago
  Trigger: Tue 2021-10-12 16:54:00 CEST; 49s left
[...]
[theo@web backup]$ systemctl is-enabled tp2_backup.timer
enabled
```
```
[theo@web backup]$ ls
[...]
tp2_backup_10-12-2021-16-03-30.tar.gz
tp2_backup_10-12-2021-16-52-17.tar.gz  
tp2_backup_10-12-2021-16-53-03.tar.gz
tp2_backup_10-12-2021-16-54-03.tar.gz                       
```

### C. Contexte

* Backup de Nextcloud

```
[theo@web ~]$ cat /etc/systemd/system/tp2_backup.service
[...]
[Service]
ExecStart=/srv/tp2_backup.sh /srv/backup /var/www/sub-domains/linux.tp2.web/html
Type=oneshot
RemainAfterExit=no
[...]
[theo@web ~]$ sudo systemctl list-timers
NEXT                          LEFT     LAST                          PASSED    UNIT                         ACTIVATES
Mon 2021-10-25 03:15:00 CEST  14h left n/a                           n/a       tp2_backup.timer             tp2_backup.service
```

Fichiers [tp2_backup.timer](./Scripts+service/tp2_backup.timer) [tp2_backup.service](./Scripts+service/tp2_backup.service)

## 5. Backup de base de données

* Base de données nextcloud

J'ai créer un nfs entre la vm backup et la vm database

```
[theo@backup ~]$ ls /srv/backup/
db.tp2.linux  web.tp2.linux
```

```
[theo@db srv]$ sudo ./tp2_backup_db.sh /srv/backup/ /var/lib/mysql/nextcloud
Enter password:
tar: Removing leading `/' from member names
[...]
[theo@db srv]$ ls
backup  nextcloud.sql  tp2_backup_10-24-2021-15-49-44.tar.gz  tp2_backup_db.sh
```
```
[theo@backup db.tp2.linux]$ ls
nextcloud.sql  tp2_backup_10-24-2021-15-49-44.tar.gz
```

Fichier [tp2_backup_db.sh](./Scripts+service/tp2_backup_db.sh)

* Réimplantation du fichier.sql dans la base de données

`[theo@db srv]$ mysql -u root -p nextcloud < nextcloud.sql`

* Vérification service+timer

```
[theo@db ~]$ systemctl enable --now tp2_backup_db.timer
[...]
[theo@db ~]$ systemctl is-enabled tp2_backup_db.service
enabled
[theo@db ~]$ sudo systemctl list-timers
NEXT                          LEFT          LAST                          PASSED      UNIT                         ACTIVATES
[...]
Mon 2021-10-25 03:30:00 CEST  11h left      n/a                           n/a         tp2_backup_db.timer          tp2_backup_db.service
[...]
```

Fichiers [tp2_backup_db.timer](./Scripts+service/tp2_backup_db.timer) [tp2_backup_db.service](./Scripts+service/tp2_backup_db.service)

# III. Reverse Proxy

## 2. Setup simple

* nginx

```
[theo@front ~]$ systemctl status nginx.service
● nginx.service - The nginx HTTP and reverse proxy server
   Loaded: loaded (/usr/lib/systemd/system/nginx.service; disabled; vendor preset: disabled)
   Active: active (running) since Sun 2021-10-24 16:16:09 CEST; 26s ago
[...]
[theo@front ~]$ systemctl enable nginx.service
```
```
PS C:\Users\theoq> curl 10.102.1.14:80
StatusCode        : 200
StatusDescription : OK
Content           : <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">

                    <html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
                      <head>
                        <title>Test Page for the Nginx...
[...]
```

* Fichier conf 

User : 

```
[theo@front ~]$ sudo cat less /etc/nginx/nginx.conf
[...]
user nginx;
[...]
```

Bloc server{} : 

```
[...]
server {
        listen       80 default_server;
        listen       [::]:80 default_server;
        server_name  _;
        root         /usr/share/nginx/html;

        # Load configuration files for the default server block.
        include /etc/nginx/default.d/*.conf;

        location / {
        }

        error_page 404 /404.html;
            location = /40x.html {
        }

        error_page 500 502 503 504 /50x.html;
            location = /50x.html {
        }
    }
[...]
```

Autres fichiers de conf :

```
[...]
include /usr/share/nginx/modules/*.conf;
include /etc/nginx/conf.d/*.conf;
include /etc/nginx/default.d/*.conf;
[...]
```

* Modif conf 

```
[theo@front ~]$ cat /etc/nginx/conf.d/web.tp2.linux.conf
server {
        # on demande à NGINX d'écouter sur le port 80 pour notre NextCloud
        listen 80;

        # ici, c'est le nom de domaine utilisé pour joindre l'application
        # ce n'est pas le nom du reverse proxy, mais le nom que les clients devront saisir pour atteindre le site
        server_name web.tp2.linux; # ici, c'est le nom de domaine utilisé pour joindre l'application (pas forcéme

        # on définit un comportement quand la personne visite la racine du site (http://web.tp2.linux/)
        location / {
                # on renvoie tout le trafic vers la machine web.tp2.linux
                proxy_pass http://web.tp2.linux;
        }
}
[theo@front ~]$ systemctl restart nginx.service
```

```
PS C:\Users\theoq> curl 10.102.1.14:80                        
StatusCode        : 200
StatusDescription : OK
Content           : <!DOCTYPE html>
                    <html>
                    <head>
                        <script> window.location.href="index.php"; </script>
                        <meta http-equiv="refresh" content="0; URL=index.php">
                    </head>
                    </html>
[...]
```

# IV. Firewalling

## 2. Mise en place

### A. Base de données

```
[theo@db ~]$ sudo firewall-cmd --get-default-zone
drop
[theo@db ~]$ sudo firewall-cmd --new-zone=ssh --permanent
success
[theo@db ~]$ sudo firewall-cmd --zone=ssh --add-service=ssh
success
[theo@db ~]$ sudo firewall-cmd --zone=ssh --add-service=ssh --permanent
success
[theo@db ~]$ sudo firewall-cmd --zone=ssh --add-port=3306/tcp
success
[theo@db ~]$ sudo firewall-cmd --zone=ssh --add-port=3306/tcp --permanent
success
[theo@db ~]$ sudo firewall-cmd --zone=ssh --add-source=10.102.1.11/32  --permanent
success
[theo@db ~]$ sudo firewall-cmd --reload
success
[theo@db ~]$ sudo firewall-cmd --zone=ssh --list-all
ssh (active)
  target: default
  icmp-block-inversion: no
  interfaces:
  sources: 10.102.1.11/32
  services: ssh
  ports: 3306/tcp
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
```

* Vérification

```
[theo@web ~]$ ping -c 2 10.102.1.12
PING 10.102.1.12 (10.102.1.12) 56(84) bytes of data.
64 bytes from 10.102.1.12: icmp_seq=1 ttl=64 time=0.646 ms
64 bytes from 10.102.1.12: icmp_seq=2 ttl=64 time=0.995 ms

--- 10.102.1.12 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1037ms
rtt min/avg/max/mdev = 0.646/0.820/0.995/0.176 ms
```

### B. Serveur Web

```
[theo@web ~]$ sudo firewall-cmd --get-default-zone
drop
[theo@db ~]$ sudo firewall-cmd --new-zone=proxy --permanent
success
[theo@web ~]$ sudo firewall-cmd --zone=proxy --add-port=80/tcp --permanent
success
[theo@web ~]$ sudo firewall-cmd --zone=proxy --add-source=10.102.1.14/32 --permanent
success
[theo@web ~]$ sudo firewall-cmd --zone=proxy --add-service=ssh --permanent
success
[theo@web ~]$ sudo firewall-cmd --reload
success
[theo@web ~]$ sudo firewall-cmd --list-all --zone=proxy
proxy (active)
  target: default
  icmp-block-inversion: no
  interfaces:
  sources: 10.102.1.14/32
  services: ssh
  ports: 80/tcp
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
[theo@web ~]$  sudo firewall-cmd --get-active-zones
drop
  interfaces: enp0s8 enp0s3
proxy
  sources: 10.102.1.14/32
```

* Vérification 

```
[theo@front ~]$ ping -c 2 10.102.1.11
PING 10.102.1.11 (10.102.1.11) 56(84) bytes of data.
64 bytes from 10.102.1.11: icmp_seq=1 ttl=64 time=0.640 ms
64 bytes from 10.102.1.11: icmp_seq=2 ttl=64 time=1.18 ms

--- 10.102.1.11 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1043ms
rtt min/avg/max/mdev = 0.640/0.909/1.179/0.271 ms
```

### C. Serveur de backup

```
[theo@backup ~]$ sudo firewall-cmd --set-default-zone=drop
success
[theo@backup ~]$ sudo firewall-cmd --new-zone=backup --permanent
success
[theo@backup ~]$ sudo firewall-cmd --add-service=nfs --zone=backup --permanent
success
[theo@backup ~]$ sudo firewall-cmd --list-all --zone=backup
backup (active)
  target: default
  icmp-block-inversion: no
  interfaces:
  sources: 10.102.1.11/32 10.102.1.12/32
  services: nfs ssh
  ports:
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
[theo@backup ~]$ sudo firewall-cmd --get-active-zones
backup
  sources: 10.102.1.11/32 10.102.1.12/32
drop
  interfaces: enp0s8 enp0s3
[theo@backup ~]$ sudo firewall-cmd --get-default-zone
drop
```

### D. Reverse Proxy

```
[theo@front ~]$ sudo firewall-cmd --set-default-zone=drop
success
[theo@front ~]$ sudo firewall-cmd --new-zone=reseau --permanent
success
[theo@front ~]$ sudo firewall-cmd --zone=reseau --add-service=ssh --permanent
success
[theo@front ~]$ sudo firewall-cmd --zone=reseau --add-source=10.102.1.0/24 --permanent
success
[theo@front ~]$ sudo firewall-cmd --reload
success
[theo@front ~]$ sudo firewall-cmd --get-default-zone
drop
[theo@front ~]$ sudo firewall-cmd --get-active-zones
drop
  interfaces: enp0s8 enp0s3
reseau
  sources: 10.102.1.0/24
[theo@front ~]$ sudo firewall-cmd --list-all --zone=reseau
reseau (active)
  target: default
  icmp-block-inversion: no
  interfaces:
  sources: 10.102.1.0/24
  services: ssh
  ports:
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
```

* Vérification

```
[theo@db ~]$ ping -c 2 10.102.1.14
PING 10.102.1.14 (10.102.1.14) 56(84) bytes of data.
64 bytes from 10.102.1.14: icmp_seq=1 ttl=64 time=0.502 ms
64 bytes from 10.102.1.14: icmp_seq=2 ttl=64 time=0.857 ms

--- 10.102.1.14 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1044ms
rtt min/avg/max/mdev = 0.502/0.679/0.857/0.179 ms
```

### E. Tableau récap

| Machine            | IP            | Service                 | Port ouvert | IPs autorisées|
|--------------------|---------------|-------------------------|-------------|---------------|
| `web.tp2.linux`    | `10.102.1.11` | Serveur Web             | 80/tcp      | 10.102.1.14/32|
| `db.tp2.linux`     | `10.102.1.12` | Serveur Base de Données | 3306/tcp    | 10.102.1.11/32|
| `backup.tp2.linux` | `10.102.1.13` | Serveur de Backup (NFS) | x           | 10.102.1.11/32, 10.102.1.12/32|
| `front.tp2.linux`  | `10.102.1.14` | Reverse Proxy           | x           | 10.102.1.0/24 |