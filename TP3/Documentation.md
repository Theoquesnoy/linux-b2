QUESNOY Théo

# TP3 : Your own shiet

# Sommaire
- [A. Netdata et les alertes](#a-netdata-et-les-alertes)
    - [1. Installation de netdata](#1-installation-de-netdata)
    - [2. Configuration de sendmail](#2-configuration-de-sendmail)
    - [3. Configuration des alertes](#3-configuration-des-alertes)
- [B. Cockpit](#b-cockpit)
    - [1. Activer cockpit](#1-activer-cockpit)
    - [2. HTTPS](#2-https)
- [C. Sécurité renforcée](#c-sécurité-renforcée)
    - [1. Désactiver ipv6](#1-désactiver-ipv6)
    - [2. SSH harden](#2-ssh-harden)
        - [2.1. Dossier .ssh](#21-dossier-ssh)
    - [3. Fail2Ban](#3-fail2ban)

Avant de commencer, téléchargez les scripts qui se trouvent [ici](./Scripts/) et exécutez le script `install_packages.sh` pour installer tous les paquests requis.

## A : Netdata et les alertes

### 1 : Installation de netdata

Tout d'abord, il faut installer netdata. Pour ce faire, vous allez devoir suivre les instructions : 

* Passez en mode root `su -`
* Executer cette commande `bash <(curl -Ss https://my-netdata.io/kickstart-static64.sh)`
* Quitter le mode root `exit`

ATTENTION : la configuration de netdata se trouvera dans le dossier /opt/netdata/etc/netdata.

Une fois l'installation finie, activer le service netdata au démarrage : `systemctl enable --now netdata`nous allons parametrer des alertes que l'on va recevoir sur notre boite mail.

### 2 : Configuration de sendmail

Pour cela, il faudra installer le paquet `sendmail` : `sudo dnf install -y sendmail`

Activez le au démarrage avec la commande : `systemctl enable --now sendmail`

Nous allons commencer par configurer sendmail.
Tout d'abord, nous allons créer une sauvegarde de la configuration actuelle.
Exécutez donc cette commande : `sudo cp /etc/mail/sendmail.mc /etc/mail/sendmail.mc.sauvegarde`

Ensuite, nous allons configurer le fichier `sendmail.mc`.
et modifier ces lignes : 

```
VERSIONID(`setup for linux')dnl
dnl define(`SMART_HOST', `smtp.your.provider')dnl
dnl MASQUERADE_AS(`mydomain.com')dnl
```
comme ceci : 

```
VERSIONID(`Installation Personnel')dnl
dnl define(`SMART_HOST',`smtp.fournisseur.fr')
MASQUERADE_AS(`fournisseur.fr')dnl
```
Où vous changerez le `fournisseur` par VOTRE fournisseur internet
Et ajouter cette ligne ```dnl FEATURE(`genericstable')dnl```

Une fois ce fichier configuré, nous allons éditer un fichier `genericstable`. Ce fichier va permettre de lier un utilisateur à une adresse mail.

Editez le de cette manière : user votreadresse@fournisseur.fr

Pour executer la commande suivant, il faudra passer en mode root. Une fois fait, executer cette commande : `m4 sendmail.mc > /etc/mail/sendmail.cf`

Vous pouvez maintenant redémarrer le service sendmail et testez l'envoi de mail avec cette commande : `echo "Subject: sendmail test" | sendmail -v votreadresse@fournisseur.fr` (n'oubliez pas de regarder dans vos spams s'il ne sont pas arrivez dans votre boite de réception)

Une fois que le test fonctionne, nous allons pouvoir paramétrer les alertes Netdata.

### 3 : Configuration des alertes

Déplacer vous dans le dossier `/opt/netdata/etc/netdata` et configurer le fichier `health_alarm_notify.conf` avec cette commande : `sudo ./edit-config health_alarm_notify.conf`

Il faudra alors trouver ces 3 lignes (vous pouvez rechercher un mot avec `/mot_recherche`) pour vérifier la bonne configuration et il faudra modifier avec votre adresse mail : 

```
sendmail="/usr/sbin/sendmail"
SEND_EMAIL="YES"
DEFAULT_RECIPIENT_EMAIL="validemail@mydomain.fr"
```

Une fois votre addresse mail paramétrée, vous pouvez exécutez le script qui va automatiser les étapes suivantes.

Redémarrer le service netdata

Ouvrir le port au niveau du firewall : `sudo firewall-cmd --add-port=19999/tcp -- permanent` et recharger sa configuration : `sudo firewall-cmd --reload`

Une fois fait, il ne vous reste plus qu'a tester le bon fonctionnement des alertes avec cette commande : `sudo /opt/netdata/usr/libexec/netdata/plugins.d/alarm-notify.sh test` (n'oubliez pas de regarder dans vos spams s'il ne sont pas arrivez dans votre boite de réception)

Si les alertes sont bien arrivés, alors votre netdata est configuré pour vous prévenir en cas de surutilisation de la RAM, du CPU...

ATTENTION, pour que les alertes fonctionne, il faut avoir un onglet avec la page web Netdata.

## B : Cockpit

### 1 : Activer cockpit

Cockpit est déjà installer sur la plupart des systèmes linux.
Il suffit donc d'activer le socket de cockpit et d'ajouter le port au firewall.

Vous pouvez exécuter le script activate_cockpit.sh

Pour ce faire, exécutez les commande suivantes : 
```
systemctl enable --now cockpit.socket
sudo firewall-cmd --add-port=9090/tcp- --permanent
sudo firewall-cmd --reload
```

### 2 : HTTPS

Nous allons commencer par générer un certificat avec une clé privée.

Tout d'abord, placez vous dans votre dossier personnel avec la commande `cd`

Ensuite, executer cette commande : `openssl req -new -newkey rsa:2048 -days 365 -nodes -x509 -keyout server.key -out server.crt`

Appuyer sur la touche entrée jusqu'a à ligne `Common Name (eg, your name or your server\'s hostname) []:` où il faudra mettre le nom de la machine virtuelle

Vous pouvez exécutez le script create_cert_cockpit.sh pour automatiser les prochaines étapes

Une fois cela terminé, nous allons créer un fichier `ssl.cert`
À l'intérieur de ce fichier, il faudra y mettre le contenu du fichier server.crt, puis le contenu de server.key.
Ce qui ressemblera à quelque chose comme ca : 

```
-----BEGIN CERTIFICATE-----
......
-----END CERTIFICATE-----
-----BEGIN PRIVATE KEY-----
......
-----END PRIVATE KEY-----
```

Ce fichier `ssl.cert`, nous allons le copier dans le dossier où cockpit stocke les certificats.
Exécutez donc cette commande : `sudo cp ssl.cert /etc/cockpit/ws-certs.d/`

Une fois le fichier copié, il faudra redémarrer cockpit : `systemctl restart cockpit.socket`
Et vérifier si cockpit a bien pris en compte le nouveau certificat : `sudo remotectl certificate`
Vous devriez apercevoir le chemin du nouveau certificat.

Rendez-vous ensuite dans un navigateur et entrez dans la barre d'URL `https://cockpit.linux.tp3:9090`
Il y aura un avertissement de sécurité, ce qui est normal car on à signé nous même le certificat. Acceptez donc de continuer et vous pourrez vous connecter avec votre user et votre mot de passe.

## C : Sécurité renforcée

### 1 : Désactiver ipv6

Vous pouvez exécutez le script desactivate_ipv6.sh

Commencons par désactiver l'ipv6.

Exécutez la commande `ip a | grep inet6` : 
Vous devriez avec quelque chose comme ça
```
inet6 ::1/128 scope host
inet6 fe80::a00:27ff:fea2:3777/64 scope link noprefixroute
inet6 fe80::a00:27ff:fe04:b61/64 scope link noprefixroute
```

Il va donc falloir créer une nouvelle config : 
`sudo vim /etc/sysctl.d/70-ipv6.conf`
et ajoutez ces lignes : 
```
net.ipv6.conf.all.disable_ipv6 = 1
net.ipv6.conf.default.disable_ipv6 = 1
```
Redémarrer le service avec la nouvelle config : `sudo sysctl --load /etc/sysctl.d/70-ipv6.conf`

Vérifier si ipv6 est bien désactiver : 
`ip a | grep inet6`
Rien ne doit s'afficher

### 2 : SSH harden

Ensuite, on va éditer le fichier de configuration `sshd_config` : `sudo vim /etc/ssh/sshd_config`

Puis modifier les lignes suivantes : 

```
PermitRootLogin yes
PasswordAuthentication yes
AllowTcpForwarding YES
ClientAliveCountMax 3
Compression DELAYED
LogLevel INFO
MaxAuthTries 6
MaxSessions 10
Port 22
TCPKeepAlive YES
X11Forwarding YES
AllowAgentForwarding YES
```
En les modifiant à ce mode de configuration :
```
PermitRootLogin no
PasswordAuthentication yes
AllowTcpForwarding NO
ClientAliveCountMax 2
Compression NO
LogLevel VERBOSE
MaxAuthTries 3
MaxSessions 2
Port 3486
TCPKeepAlive NO
X11Forwarding NO
AllowAgentForwarding NO
```
(vous modifirais le `PasswordAuthentication no` plus tard, après avoir généré les clés ssh)

Ouvrer ensuite le port que vous avez choisi avec la commande : `sudo firewall-cmd --add-port=3486/tcp --permanent`,puis recharger la conf du firewall : `sudo firewall-cmd --reload`

Redémarrer le service ssh : `systemctl restart sshd`

Essayer maintenant de vous connecter avec cette commande là : 
`ssh username@ip_vm -p 3486`

Nous allons maintenant générer des clés SSH.

Executer sur VOTRE pc la commande `ssh-keygen` et suivés les instructions
Une fois les clés générées, la suite va dépendre selon votre OS

* Sur linux : 

Pour copier la clé publique sur la machine où l'on veut se connecter, il faudra executer la commande`ssh-copy-id username@adress_ip_VM`
Suiviez les instructions et le nombre de clé sera ajouté de 1.

Si rien ne se passe, c'est que le dossier dans lequel la clé doit être copiée n'existe pas, il faudra alors le créer SUR LA MACHINE VIRTUELLE (référez-vous à la partie [2.1 Dossier .ssh](#2.1--Dossier-.ssh))

Relancez la commande `ssh-copy-id`

Retournez dans le fichier sshd_config `sudo vim /etc/ssh/sshd_config` et changez `PasswordAuthentication yes` en `PasswordAuthentication no`

Essayer maintenant de vous connecter et aucun mot de passe ne vous sera demandé


* Sur windows : 

Ouvrez un powershell et exécutez cette commande : `cat C:\Users\votre_user\.ssh\id_rsa.pub | ssh user_vm@ip_vm -p port_ssh "cat >> ~/.ssh/authorized_keys"`

Si le dossier n'existe pas, référez-vous à la partie [2.1 Dossier .ssh](#2.1--Dossier-.ssh)

Une fois la clé copiée, retournez dans le fichier sshd_config `sudo vim /etc/ssh/sshd_config` et changez `PasswordAuthentication yes` en `PasswordAuthentication no`

Essayer maintenant de vous connecter et aucun mot de passe ne vous sera demandé

#### 2.1 : Dossier .ssh

Vous pouvez exécutez le script create_directory_ssh.sh

On se place dans notre répertoire personnel avec la commande `cd`

On créer le répertoire `.ssh` et on place dedans : 
```
mkdir .ssh
cd .ssh
```

On créer un fichier `authorized_keys` dans lequel la clé doit être copiée :
`vim authorized_keys`

Définissez les droits sur le dossier et le fichier : 
```
sudo chmod 600 /home/user/.ssh/authorized_keys
sudo chmod 700 /home/user/.ssh
```

### 3 : Fail2Ban
 
Avant d'installer fai2ban, il faut dans un premier temps installer le paquet `epel-release` : `sudo dnf install -y epel-release`
 
On peut maintenant installer fail2ban : `sudo dnf install fail2ban fail2ban-firewalld`

Lancez-le et activez le au démarrage : `sudo systemctl start fail2ban.service` et `sudo systemctl enable fail2ban.service`

Nous allons modifer la conf. Cependant, il ne faut jamais modifer le `jail.conf` directement. On va donc créer une copie `jail.local` et modifer ce fichier

ajoutez ces lignes dans la partie [default] : 
```
findtime = 1h
maxretry = 5
```

Configurer avec le firewall : `sudo mv /etc/fail2ban/jail.d/00-firewalld.conf /etc/fail2ban/jail.d/00-firewalld.local`
et redémarrer le service fail2ban

Nous allons maintenant le configurer pour le sshd :
Vous pouvez exécuter le script fail2ban_sshd.sh

Créer le fichier sshd.local : `sudo vim /etc/fail2ban/jail.d/sshd.local`
Ajoutez ces lignes : 
```
[sshd]
enabled = true

# Override the default global configuration
# for specific jail sshd
bantime = 1d
maxretry = 3
```

Redémarrer le service et vérifier si sshd est bien pris en compte avec cette commande : `sudo fail2ban-client status`
Vous devriez alors voir le sshd dans la `jail-list`