QUESNOY Théo

# TP2 pt. 1 : Gestion de service

# Sommaire

- [TP2 pt. 1 : Gestion de service](#tp2-pt-1--gestion-de-service)
- [Sommaire](#sommaire)
- [0. Prérequis](#0-prérequis)
  - [Checklist](#checklist)
- [I. Un premier serveur web](#i-un-premier-serveur-web)
  - [1. Installation](#1-installation)
  - [2. Avancer vers la maîtrise du service](#2-avancer-vers-la-maîtrise-du-service)
- [II. Une stack web plus avancée](#ii-une-stack-web-plus-avancée)
  - [1. Intro](#1-intro)
  - [2. Setup](#2-setup)
    - [A. Serveur Web et NextCloud](#a-serveur-web-et-nextcloud)
    - [B. Base de données](#b-base-de-données)
    - [C. Finaliser l'installation de NextCloud](#c-finaliser-linstallation-de-nextcloud)

# I. Un premier serveur web

## 1. Installation
Lancement service httpd

`[theo@web ~]$ sudo systemctl start httpd`

Démarrage service httpd au boot de la machine

```
[theo@web ~]$ sudo systemctl enable httpd.service
[sudo] password for theo:
Created symlink /etc/systemd/system/multi-user.target.wants/httpd.service → /usr/lib/systemd/system/httpd.service.
```

Ouverture port du service httpd

```
[theo@web ~]$ sudo firewall-cmd --zone=public --add-port=80/tcp
success
[theo@web ~]$ sudo firewall-cmd --zone=public --add-port=80/tcp --permanent
success
```

Commande ss 
```
[theo@web ~]$ sudo ss -alnpt
State           Recv-Q          Send-Q                   Local Address:Port                   Peer Address:Port         Process
[...]
LISTEN          0               128                                  *:80                                *:*             users:(("httpd",pid=25239,fd=4)
[...]
```

Service démarré 
```
[theo@web ~]$ sudo systemctl status httpd
[sudo] password for theo:
● httpd.service - The Apache HTTP Server
   Loaded: loaded (/usr/lib/systemd/system/httpd.service; enabled; vendor preset: disabled)
   Active: active (running) since Sun 2021-10-03 13:46:21 CEST; 45s ago
     Docs: man:httpd.service(8)
 Main PID: 839 (httpd)
   Status: "Running, listening on: port 80"
    Tasks: 213 (limit: 4934)
[...]
```

Vérification avec curl localhost : 
```
[theo@web ~]$ curl localhost
<!doctype html>
<html>
  <head>
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <title>HTTP Server Test Page powered by: Rocky Linux</title>
    <style type="text/css">
      /*<![CDATA[*/
[...]
```

## 2. Avancer vers la maîtrise du service

* Commande qui permet de démarrer Apache au démarrage : `sudo systemctl enable httpd.service`

* Le service `httpd.service` démarre bien au démarrage : 
```
[theo@web ~]$ sudo systemctl list-unit-files --type=service | grep 'httpd.service'
httpd.service                              enabled
```

* Fichier httpd.service : 
```
[theo@web ~]$ cat /usr/lib/systemd/system/httpd.service
[Unit]
Description=The Apache HTTP Server
Wants=httpd-init.service
After=network.target remote-fs.target nss-lookup.target httpd-init.service
Documentation=man:httpd.service(8)

[Service]
Type=notify
Environment=LANG=C

ExecStart=/usr/sbin/httpd $OPTIONS -DFOREGROUND
ExecReload=/usr/sbin/httpd $OPTIONS -k graceful
# Send SIGWINCH for graceful stop
KillSignal=SIGWINCH
KillMode=mixed
PrivateTmp=true

[Install]
WantedBy=multi-user.target
```

* Utilisateur définit : 
```
[theo@web ~]$ sudo cat /etc/httpd/conf/httpd.conf | grep 'User'
User apache
```

* Processus en cours : 

```
[theo@web ~]$ ps -ef
[...]
apache      1529    1528  0 14:09 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache      1530    1528  0 14:09 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache      1531    1528  0 14:09 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache      1532    1528  0 14:09 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
[...]
```

* Contenu accesible : 
```
[theo@web ~]$ ls -la /usr/share/testpage/
total 12
drwxr-xr-x.  2 root root   24 Sep 29 17:27 .
drwxr-xr-x. 89 root root 4096 Sep 29 17:28 ..
-rw-r--r--.  1 root root 7621 Jun 11 17:23 index.html
```
Les utiilisateurs peuvent lire le fichier index.html car les permissions de lecture sont autorisées partout


* Nouveau utilisateur : 
```
[theo@web ~]$ sudo cat /etc/passwd
[...]
theo:x:1000:1000:theo:/home/theo:/bin/bash
apache:x:48:48:Apache:/usr/share/httpd:/sbin/nologin
ApacheUser:x:1001:1001::/usr/share/httpd/:/bin/bash
```

* Utilisateur modifié dans le fichier de conf : 

```
[theo@web ~]$ ps -ef
[...]
ApacheU+    2111    2110  0 14:49 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
ApacheU+    2112    2110  0 14:49 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
ApacheU+    2113    2110  0 14:49 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
ApacheU+    2114    2110  0 14:49 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
[...]
```

* Modification port apache : 

```
[theo@web ~]$ sudo cat /etc/httpd/conf/httpd.conf | grep 'Listen'
[sudo] password for theo:
Listen 100
```

* Suppression de l'ancien port : 

```
[theo@web ~]$ sudo firewall-cmd --remove-port=80/tcp
success
[theo@web ~]$ sudo firewall-cmd --remove-port=80/tcp --permanent
success
[theo@web ~]$ sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources:
  services: ssh
  ports:
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
```

* Ajout du port dans le firewall : 

```
[theo@web ~]$ sudo firewall-cmd --add-port=100/tcp
success
[theo@web ~]$ sudo firewall-cmd --add-port=100/tcp --permanent
success
```

* Vérification port avec ss

```
[theo@web ~]$ sudo ss -alnpt | grep '100'
LISTEN 0      128                *:100             *:*    users:(("httpd",pid=3345,fd=4),("httpd",pid=3344,fd=4),("httpd",pid=3343,fd=4),("httpd",pid=3340,fd=4))
```

* Vérification avec curl : 

```
[theo@web ~]$ curl localhost:100
<!doctype html>
<html>
  <head>
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <title>HTTP Server Test Page powered by: Rocky Linux</title>
    <style type="text/css">
      /*<![CDATA[*/
[...]
```
Fichier [Apache_httpd.conf](./Apache_httpd.conf)

# II. Une stack web plus avancée

## 2. Setup

### A. Serveur Web et NextCloud

Listes des commandes réalisées : 

```
sudo dnf install -y epel-release
sudo dnf update
sudo dnf install https://rpms.remirepo.net/enterprise/remi-release-8.rpm
sudo dnf module list php
sudo dnf -y module enable php:remi-7.4
sudo dnf -y module list php
sudo dnf install -y wget zip unzip libxml2 openssl php74-php php74-php-ctype php74-php-curl php74-php-gd php74-php-iconv php74-php-json php74-php-libxml php74-php-mbstring php74-php-openssl php74-php-posix php74-php-session php74-php-xml php74-php-zip php74-php-zlib php74-php-pdo php74-php-mysqlnd php74-php-intl php74-php-bcmath php74-php-gmp
sudo systemctl enable httpd
ls /etc/httpd/
sudo mkdir /etc/httpd/sites-available
sudo vim /etc/httpd/sites-available/linux.tp2.web
sudo mkdir /etc/httpd/sites-enabled
sudo ln -s /etc/httpd/sites-available/linux.tp2.web /etc/httpd/sites-enabled/
sudo mkdir -p /var/www/sub-domains/linux.tp2/html
timedatectl
sudo vim /etc/opt/remi/php74/php.ini
sudo wget https://download.nextcloud.com/server/releases/nextcloud-21.0.1.zip
sudo unzip nextcloud-21.0.1.zip
cd nextcloud/
sudo cp -Rf * /var/www/sub-domains/linux.tp2/html/
sudo chown -Rf apache.apache /var/www/sub-domains/linux.tp2/html/
systemctl restart httpd
```
Fichier [Nextcloud_httpd.conf](./Nextcloud_httpd.conf)
Fichier [linux.tp2.web](./linux.tp2.web)

### B. Base de données

Listes des commandes réalisées : 

```
sudo dnf install -y mariadb-server vim wget zip unzip libxml2 openssl php74-php php74-php-ctype php74-php-curl php74-php-gd php74-php-iconv php74-php-json php74-php-libxml php74-php-mbstring php74-php-openssl php74-php-posix php74-php-session php74-php-xml php74-php-zip php74-php-zlib php74-php-pdo php74-php-mysqlnd php74-php-intl php74-php-bcmath php74-php-gmp
sudo dnf install -y mariadb-server
systemctl enable mariadb
systemctl start mariadb
mysql_secure_installation
sudo ss -alnpt
```

Commande ss : 

```
[theo@db ~]$ sudo ss -alnpt
[sudo] password for theo:
State      Recv-Q     Send-Q         Local Address:Port         Peer Address:Port    Process
[...]
LISTEN     0          80                         *:3306                    *:*        users:(("mysqld",pid=4715,fd=21))
```

* Exploration base de données depuis web.tp2.linux : 

```
[theo@web ~]$ mysql -u nextcloud -h 10.102.1.12 -p
Enter password:
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 23
Server version: 10.3.28-MariaDB MariaDB Server

Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

MariaDB [(none)]> SHOW DATABASES;
+--------------------+
| Database           |
+--------------------+
| information_schema |
| nextcloud          |
+--------------------+
2 rows in set (0.001 sec)

MariaDB [(none)]> USE nextcloud
Database changed
MariaDB [nextcloud]> SHOW tables;
Empty set (0.001 sec)
```

Liste des utilisateurs de la base de données : 

```
MariaDB [(none)]> SELECT User FROM mysql.user;
+-----------+
| User      |
+-----------+
| nextcloud |
| root      |
| root      |
| root      |
+-----------+
4 rows in set (0.000 sec)
```

### C. Finaliser l'installation de NextCloud

Vérification de nextcloud avec la commande curl : 
```
PS C:\Users\theoq> curl web.tp2.linux


StatusCode        : 200
StatusDescription : OK
Content           : <!DOCTYPE html>
                    <html>
                    <head>
                        <script> window.location.href="index.php"; </script>
                        <meta http-equiv="refresh" content="0; URL=index.php">
                    </head>
                    </html>

RawContent        : HTTP/1.1 200 OK
                    Keep-Alive: timeout=5, max=100
                    Connection: Keep-Alive
                    Accept-Ranges: bytes
                    Content-Length: 156
                    Content-Type: text/html; charset=UTF-8
                    Date: Wed, 06 Oct 2021 15:24:34 GMT
                    ETag: "...
Forms             : {}
Headers           : {[Keep-Alive, timeout=5, max=100], [Connection, Keep-Alive], [Accept-Ranges, bytes],
                    [Content-Length, 156]...}
Images            : {}
InputFields       : {}
Links             : {}
ParsedHtml        : mshtml.HTMLDocumentClass
RawContentLength  : 156
```

