#!/bin/bash
# Certificate for HTTPS cockpit
# theo ~ 14/11/2021

touch ssl.cert
cat server.crt > ssl.cert
cat server.key >> ssl.cert
cp ssl.cert /etc/cockpit/ws-certs.d/
systemctl restart cockpit.socket

