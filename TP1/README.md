# TP1 Linux

## 0.Préparation de la machine

* Accès à internet

La carte réseau NAT avec `ip a`: 
```
[theo@node1 ~]$ ip a
[...]
2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:b8:a5:25 brd ff:ff:ff:ff:ff:ff
    inet 10.0.2.15/24 brd 10.0.2.255 scope global dynamic noprefixroute enp0s3
       valid_lft 85710sec preferred_lft 85710sec
    inet6 fe80::a00:27ff:feb8:a525/64 scope link noprefixroute
       valid_lft forever preferred_lft forever
[...]
```

Route par défaut avec `ip r s`: 
```
[theo@node1 ~]$ ip r s
default via 10.0.2.2 dev enp0s3 proto dhcp metric 100
10.0.2.0/24 dev enp0s3 proto kernel scope link src 10.0.2.15 metric 100
```

ping 8888 et google.com : 
```
[theo@node1 ~]$ ping 8.8.8.8
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=113 time=69.6 ms
64 bytes from 8.8.8.8: icmp_seq=2 ttl=113 time=56.2 ms
^C
--- 8.8.8.8 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1002ms
rtt min/avg/max/mdev = 56.246/62.906/69.567/6.665 ms
[theo@node1 ~]$ ping google.com
PING google.com (172.217.22.142) 56(84) bytes of data.
64 bytes from par21s12-in-f14.1e100.net (172.217.22.142): icmp_seq=1 ttl=113 time=59.9 ms
64 bytes from par21s12-in-f14.1e100.net (172.217.22.142): icmp_seq=2 ttl=113 time=93.5 ms
^C
--- google.com ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1001ms
rtt min/avg/max/mdev = 59.858/76.682/93.507/16.826 ms
```

* Réseau local

VM 1 : 

Modifier le fichier avec la commande `sudo nano /etc/sysconfig/network-scripts/ifcfg-enp0s8`
puis modifier les éléments : 

```
[theo@node1 ~]$ cat /etc/sysconfig/network-scripts/ifcfg-enp0s8
TYPE=Ethernet
PROXY_METHOD=none
BROWSER_ONLY=no
BOOTPROTO=static
DEFROUTE=yes
IPV4_FAILURE_FATAL=no
IPV6INIT=yes
IPV6_AUTOCONF=yes
IPV6_DEFROUTE=yes
IPV6_FAILURE_FATAL=no
UUID=ebf8f526-39f4-42c5-9c91-b6fb574a8ede
NAME=enp0s8
DEVICE=enp0s8
ONBOOT=yes
IPADDR=10.101.1.11
NETMASK=255.255.255.0
GATEWAY=10.10.1.254
DNS1=1.1.1.1
```

Il faut ensuite redémarrer l'interface avec la commande `sudo nmcli con reload`, puis redémarrer la carte en question `sudo nmcli con up enp0s8`

```
[theo@node1 ~]$ sudo nmcli con reload
[theo@node1 ~]$ sudo nmcli con up enp0s8
Connection successfully activated (D-Bus active path: /org/freedesktop/NetworkManager/ActiveConnection/3)
```

Et la carte est activée : 
```
[theo@node1 ~]$ ip a
[...]
3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:f2:71:f6 brd ff:ff:ff:ff:ff:ff
    inet 10.101.1.11/24 brd 10.10.1.255 scope global noprefixroute enp0s8
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:fef2:71f6/64 scope link noprefixroute
       valid_lft forever preferred_lft forever
```

VM 2 : 

Faire les mêmes opérations sur la 2ème VM

La carte est maintenant activée : 
```
[theo@node1 ~]$ ip a
[...]
3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:49:a9:1c brd ff:ff:ff:ff:ff:ff
    inet 10.101.1.12/24 brd 10.10.1.255 scope global noprefixroute enp0s8
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:fe49:a91c/64 scope link noprefixroute
       valid_lft forever preferred_lft forever
```

Les pings entre les 2 VM fonctionnent : 

Ping vers 10.101.1.11
```
[theo@node2 ~]$ ping 10.101.1.11
PING 10.101.1.11 (10.101.1.11) 56(84) bytes of data.
64 bytes from 10.101.1.11: icmp_seq=1 ttl=64 time=0.823 ms
64 bytes from 10.101.1.11: icmp_seq=2 ttl=64 time=0.919 ms
64 bytes from 10.101.1.11: icmp_seq=3 ttl=64 time=0.892 ms
64 bytes from 10.101.1.11: icmp_seq=4 ttl=64 time=0.930 ms
^C
--- 10.101.1.11 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3006ms
rtt min/avg/max/mdev = 0.823/0.891/0.930/0.041 ms
```

Ping vers 10.10.1.12
```
[theo@node1 ~]$ ping 10.101.1.12
PING 10.101.1.12 (10.101.1.12) 56(84) bytes of data.
64 bytes from 10.101.1.12: icmp_seq=1 ttl=64 time=0.524 ms
64 bytes from 10.101.1.12: icmp_seq=2 ttl=64 time=1.00 ms
64 bytes from 10.101.1.12: icmp_seq=3 ttl=64 time=1.09 ms
64 bytes from 10.101.1.12: icmp_seq=4 ttl=64 time=1.01 ms
^C
--- 10.101.1.12 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3066ms
rtt min/avg/max/mdev = 0.524/0.906/1.089/0.225 ms
```


* Nom de la machine

Changer le nom de domaine, éditer le fichier hostname avec la commande `sudo nano /etc/hostname` 

Pour le changer immédiatemment, faire la commande `sudo hostname node1.tp1.b2`
Vérifier les changements avec `hostname`
```
[theo@node1 ~]$ hostname
node1.tp1.b2
```

Faire les mêmes opérations pour la 2ème VM 
```
[theo@node2 ~]$ hostname
node2.tp1.b2
```

* DNS

Ajout du serveur DNS 1.1.1.1 dans le fichier `/etc/sysconfig/network-scripts/ifcfg-enp0s8` :

```
[theo@node1 ~]$ cat /etc/sysconfig/network-scripts/ifcfg-enp0s8
[...]
GATEWAY=10.10.1.254
DNS1=1.1.1.1
```

adresse ip du serveur qui a répondu a voir en profondeur 
```
[theo@node1 ~]$ dig ynov.com

[...]
;; QUESTION SECTION:
;ynov.com.                      IN      A
```
La réponse du serveur 
```
;; ANSWER SECTION:
ynov.com.               3820    IN      A       92.243.16.143
[...]
```
* Ping VM avec leur nom

J'ai ajouté la ligne `10.101.1.12 node2.tp1.b2` dans le fichier /etc/hosts 
Ping vers la node2 : 
```
[theo@node1 ~]$ [theo@node1 ~]$ ping node2.tp1.b2
PING node2.tp1.b2 (10.101.1.12) 56(84) bytes of data.
64 bytes from node2.tp1.b2 (10.101.1.12): icmp_seq=1 ttl=64 time=0.517 ms
64 bytes from node2.tp1.b2 (10.101.1.12): icmp_seq=2 ttl=64 time=1.12 ms
64 bytes from node2.tp1.b2 (10.101.1.12): icmp_seq=3 ttl=64 time=1.09 ms
64 bytes from node2.tp1.b2 (10.101.1.12): icmp_seq=4 ttl=64 time=0.969 ms
^C
--- node2.tp1.b2 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3052ms
rtt min/avg/max/mdev = 0.517/0.925/1.122/0.243 ms
```

J'ai ajouté la ligne `10.101.1.11 node1.tp1.b2` dans le fichier /etc/hosts
Ping vers la node1 : 
```
[theo@node2 ~]$ [theo@node2 ~]$ ping node1.tp1.b2
PING node1.tp1.b2 (10.101.1.11) 56(84) bytes of data.
64 bytes from node1.tp1.b2 (10.101.1.11): icmp_seq=1 ttl=64 time=0.577 ms
64 bytes from node1.tp1.b2 (10.101.1.11): icmp_seq=2 ttl=64 time=1.15 ms
64 bytes from node1.tp1.b2 (10.101.1.11): icmp_seq=3 ttl=64 time=0.961 ms
64 bytes from node1.tp1.b2 (10.101.1.11): icmp_seq=4 ttl=64 time=1.18 ms
^C
--- node1.tp1.b2 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3056ms
rtt min/avg/max/mdev = 0.577/0.965/1.175/0.239 ms
```

* Pare-feu activé 

Pour voir le firewall activé, la commande `sudo firewall-cmd --list-all` permet de le voir

```
[theo@node1 ~]$ sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources:
  services: cockpit dhcpv6-client ssh
  ports:
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
```
Egalement pour node2 
```
[theo@node2 ~]$ sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources:
  services: cockpit dhcpv6-client ssh
  ports:
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
```

## I. Utilisateurs

### 1. Création et configuration

L'utilisateur newuser1 a été créer dans le répertoire home (-m) avec comme shell bin/bash : 
```
[theo@node1 ~]$ sudo useradd newuser1 -m -s /bin/bash
[theo@node1 ~]$ ls -ls /home
total 0
0 drwx------. 2 newuser1 newuser1 62 Sep 22 16:58 newuser1
0 drwx------. 2 theo     theo     83 Sep 15 17:47 theo
```

Nouveau groupe créer avec `[theo@node1 ~]$ sudo groupadd admins`
Puis ajout de la ligne`%admins ALL=(ALL)       ALL` dans le fichier /etc/sudoers via `sudo visudo`

Ajout du nouvel utilisateur dans le groupe `admins` : 
`[theo@node1 ~]$ sudo usermod -a -G admins newuser1`

Puis vérification : 
```
[theo@node1 ~]$ groups newuser1
newuser1 : newuser1 admins
```

### 2.SSH

Création des clés SSH (mis en place d'un mdp sur la clé): 

```
C:\Users\theoq>ssh-keygen -t rsa -b 4096
Generating public/private rsa key pair.
Enter file in which to save the key (C:\Users\theoq/.ssh/id_rsa):
Enter passphrase (empty for no passphrase):
Enter same passphrase again:
Your identification has been saved in C:\Users\theoq/.ssh/id_rsa.
Your public key has been saved in C:\Users\theoq/.ssh/id_rsa.pub.
[...]
```

Clé publique sur la VM : 
```
[newuser1@node1 .ssh]$ cat authorized_keys
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDbuXNE2+WZJqQ5S4Wiu+qz8ZX6k9FMAsdx+JCJzTNGrqlCcLR4cJhG5WJlXmxtkDMoPjPCHTuxdDRO0PN0SHk/nuZCfFd9znKyz6XQlifYBnLCv9rqcksUwSQ6DvtrgB/ermz3m/1VlOsp5J8y2KAoLXy56BcQ6j7ih35QDQH3hmtfL8hAnZXZoIR3eakggEgMA7mc8YXGx/u4jM5OeKCLnAAvbh9+W1xhKTOlmDRVuFEDBgw4RaNQYEQL6LbkdRRtUsDaqxRrAIcDEihGWdwaEda5q9X0SOEpLcyNYrMTuN726a1lEkQcobVGcVXitYMXNzfffvzEcQVdWfXGBb42tDWmqBEcDcP6pvu18bygsSEbS5O8yJVvnbmvtYv4lcMB++DDaBWrobey3QGY29h4yNukW77sZTicJUZpgqnL5+32L7PkeuZTOQnbBZz9MJwiw0F36sSkqdQ2Orxzo3+5eHjwLleFUEEW7TV8NH+VJxvQihnqSDNmd/2N/vyhX6kGOWBp9vwC9LlK1oJvTv8B2u6OeMGZeL6ptIBPtlau3aEAb4sZNLDpkJOEeei/Xu5LXcRup3ViK3p0NXe2ctUmk+1HRhC4e4k9qDAC/rQhB2D21klyfgeKTDy0m8kHrbI/HJpOyFoE7qqwErfI9UoG0rAoAOwSkph1Gq+euVlj9Q== theoq@LENOVO-Théo
```

Changement de droits du dossier et du fichier : 
```
[newuser1@node1 .ssh]$ chmod 600 authorized_keys
[newuser1@node1 .ssh]$ ls -la
total 4
drwxrwxr-x. 2 newuser1 newuser1  29 Sep 25 11:39 .
drwx------. 3 newuser1 newuser1  90 Sep 25 11:39 ..
-rw-------. 1 newuser1 newuser1 744 Sep 25 11:39 authorized_keys
[newuser1@node1 .ssh]$ cd ..
[newuser1@node1 ~]$ chmod 700 .ssh/
[newuser1@node1 ~]$ ls -la
total 16
drwx------. 3 newuser1 newuser1  90 Sep 25 11:39 .
drwxr-xr-x. 4 root     root      34 Sep 22 16:58 ..
[...]
drwx------. 2 newuser1 newuser1  29 Sep 25 11:39 .ssh
-rw-------. 1 newuser1 newuser1 804 Sep 25 11:39 .viminfo
```

Connexion SSH : 
```
C:\Users\theoq>ssh newuser1@10.101.1.11
Enter passphrase for key 'C:\Users\theoq/.ssh/id_rsa':
Activate the web console with: systemctl enable --now cockpit.socket

Last login: Sat Sep 25 11:50:08 2021 from 10.101.1.1
[newuser1@node1 ~]$
```


## II. Partitionnement

### 2. Partitionnement

* Création group data
```
[theo@node1 ~]$ sudo vgcreate data /dev/sdb
[sudo] password for theo:
  Physical volume "/dev/sdb" successfully created.
  Volume group "data" successfully created
[theo@node1 ~]$ sudo vgextend data /dev/sdc
  Physical volume "/dev/sdc" successfully created.
  Volume group "data" successfully extended
[theo@node1 ~]$ sudo vgs
  VG   #PV #LV #SN Attr   VSize  VFree
  data   2   0   0 wz--n-  5.99g 5.99g
  rl     1   2   0 wz--n- <7.00g    0
```
* Création logical volume

```
[theo@node1 ~]$ sudo lvcreate -L 1G data -n f_data_lv
  Logical volume "f_data_lv" created.
[theo@node1 ~]$ sudo lvcreate -L 1G data -n s_data_lv
  Logical volume "s_data_lv" created.
[theo@node1 ~]$ sudo lvcreate -L 1G data -n t_data_lv
  Logical volume "t_data_lv" created.
[theo@node1 ~]$ sudo lvs
  LV        VG   Attr       LSize   Pool Origin Data%  Meta%  Move Log Cpy%Sync Convert
  f_data_lv data -wi-a-----   1.00g
  s_data_lv data -wi-a-----   1.00g
  t_data_lv data -wi-a-----   1.00g
  root      rl   -wi-ao----  <6.20g
  swap      rl   -wi-ao---- 820.00m
```

* Formatage des LV en format ext4

```
[theo@node1 ~]$ sudo mkfs -t ext4 /dev/data/f_data_lv
mke2fs 1.45.6 (20-Mar-2020)
Creating filesystem with 262144 4k blocks and 65536 inodes
Filesystem UUID: 2119a872-b988-4fc2-8094-f680ba43116f
Superblock backups stored on blocks:
        32768, 98304, 163840, 229376

Allocating group tables: done
Writing inode tables: done
Creating journal (8192 blocks): done
Writing superblocks and filesystem accounting information: done
[theo@node1 ~]$ sudo mkfs -t ext4 /dev/data/s_data_lv
mke2fs 1.45.6 (20-Mar-2020)
Creating filesystem with 262144 4k blocks and 65536 inodes
Filesystem UUID: e85bfd12-80a0-482f-af58-4d974500abf6
Superblock backups stored on blocks:
        32768, 98304, 163840, 229376

Allocating group tables: done
Writing inode tables: done
Creating journal (8192 blocks): done
Writing superblocks and filesystem accounting information: done

[theo@node1 ~]$ sudo mkfs -t ext4 /dev/data/t_data_lv
mke2fs 1.45.6 (20-Mar-2020)
Creating filesystem with 262144 4k blocks and 65536 inodes
Filesystem UUID: e56fc51b-da4f-4f40-a99c-6cd521cdbb70
Superblock backups stored on blocks:
        32768, 98304, 163840, 229376

Allocating group tables: done
Writing inode tables: done
Creating journal (8192 blocks): done
Writing superblocks and filesystem accounting information: done
```

* Montage des disques 

```
[theo@node1 ~]$ sudo mkdir /mnt/part1
[theo@node1 ~]$ sudo mkdir /mnt/part2
[theo@node1 ~]$ sudo mkdir /mnt/part3
[theo@node1 ~]$ sudo mount /dev/data/f_data_lv /mnt/part1/
[theo@node1 ~]$ sudo mount /dev/data/s_data_lv /mnt/part2/
[theo@node1 ~]$ sudo mount /dev/data/t_data_lv /mnt/part3/
[theo@node1 ~]$ df -h
Filesystem                  Size  Used Avail Use% Mounted on
devtmpfs                    890M     0  890M   0% /dev
tmpfs                       909M     0  909M   0% /dev/shm
tmpfs                       909M  8.6M  901M   1% /run
tmpfs                       909M     0  909M   0% /sys/fs/cgroup
/dev/mapper/rl-root         6.2G  1.9G  4.4G  31% /
/dev/sda1                  1014M  182M  833M  18% /boot
tmpfs                       182M     0  182M   0% /run/user/1000
/dev/mapper/data-f_data_lv  976M  2.6M  907M   1% /mnt/part1
/dev/mapper/data-s_data_lv  976M  2.6M  907M   1% /mnt/part2
/dev/mapper/data-t_data_lv  976M  2.6M  907M   1% /mnt/part3
```

* Montage automatique (/etc/fstab)

```
[theo@node1 ~]$ sudo vim /etc/fstab
[theo@node1 ~]$ sudo umount /mnt/part1
[theo@node1 ~]$ sudo umount /mnt/part2
[theo@node1 ~]$ sudo umount /mnt/part3
[theo@node1 ~]$ sudo mount -av
[...]
/mnt/part1               : successfully mounted
mount: /mnt/part2 does not contain SELinux labels.
       You just mounted an file system that supports labels which does not
       contain labels, onto an SELinux box. It is likely that confined
       applications will generate AVC messages and not be allowed access to
       this file system.  For more details see restorecon(8) and mount(8).
/mnt/part2               : successfully mounted
mount: /mnt/part3 does not contain SELinux labels.
       You just mounted an file system that supports labels which does not
       contain labels, onto an SELinux box. It is likely that confined
       applications will generate AVC messages and not be allowed access to
       this file system.  For more details see restorecon(8) and mount(8).
/mnt/part3               : successfully mounted
```

## III. Gestion de services

### 1. Interaction avec un service existant

Vérification firewalld : 

```
[theo@node1 ~]$ systemctl is-active firewalld
active
[theo@node1 ~]$ systemctl is-enabled firewalld
enabled
```

### 2. Création de service

#### A. Unité simpliste

Ouverture port 8888 : 
```
[theo@node1 ~]$ sudo firewall-cmd --add-port=8888/tcp
success
[theo@node1 ~]$ sudo firewall-cmd --reload
success
```
Fichier créer puis reload : 
```
[theo@node1 ~]$ sudo systemctl daemon-reload
[theo@node1 ~]$ sudo systemctl status web.service
● web.service - Very simple web service
   Loaded: loaded (/etc/systemd/system/web.service; enabled; vendor preset: disabled)
   Active: active (running) since Sat 2021-09-25 15:47:25 CEST; 2min 24s ago
 Main PID: 874 (python3)
    Tasks: 1 (limit: 11385)
   Memory: 16.1M
   CGroup: /system.slice/web.service
           └─874 /bin/python3 -m http.server 8888

Sep 25 15:47:25 node1.tp1.b2 systemd[1]: Started Very simple web service.
[theo@node1 ~]$ sudo systemctl start web.service
[theo@node1 ~]$ sudo systemctl enable web.service
[theo@node1 ~]$ curl 10.101.1.11:8888
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Directory listing for /</title>
</head>
<body>
<h1>Directory listing for /</h1>
<hr>
<ul>
<li><a href="bin/">bin@</a></li>
<li><a href="boot/">boot/</a></li>
<li><a href="dev/">dev/</a></li>
<li><a href="etc/">etc/</a></li>
<li><a href="home/">home/</a></li>
<li><a href="lib/">lib@</a></li>
<li><a href="lib64/">lib64@</a></li>
<li><a href="media/">media/</a></li>
<li><a href="mnt/">mnt/</a></li>
<li><a href="opt/">opt/</a></li>
<li><a href="proc/">proc/</a></li>
<li><a href="root/">root/</a></li>
<li><a href="run/">run/</a></li>
<li><a href="sbin/">sbin@</a></li>
<li><a href="srv/">srv/</a></li>
<li><a href="sys/">sys/</a></li>
<li><a href="tmp/">tmp/</a></li>
<li><a href="usr/">usr/</a></li>
<li><a href="var/">var/</a></li>
</ul>
<hr>
</body>
</html>
```
#### B. Modification de l'unité

Utilisateur `web` créer  : 
`[theo@node1 ~]$ sudo adduser web -m -s /bin/sh`

Modification de l'unité : 

```
[theo@node1 testWeb]$ cat /etc/systemd/system/web.service
[Unit]
Description=Very simple web service

[Service]
ExecStart=/bin/python3 -m http.server 8888
User=web
WorkingDirectory=/srv/testWeb


[Install]
WantedBy=multi-user.target
```

Ajout d'un dossier est d'un fichier de l'utilisateur `web` : 

```
[web@node1 ~]$ cd /srv/testWeb/
[web@node1 testWeb]$ ls
testFichier
```
Récupération du fichier `testFichier` : 

```
[theo@node1 ~]$ curl 10.101.1.11:8888
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Directory listing for /</title>
</head>
<body>
<h1>Directory listing for /</h1>
<hr>
<ul>
<li><a href="testFichier">testFichier</a></li>
</ul>
<hr>
</body>
</html>
```