#!/bin/bash
# Test configuration netdata
# theo ~ 14/11/2021

systemctl restart netdata.service
firewall-cmd --add-port=19999/tcp --permanent
firewall-cmd --reload
/opt/netdata/usr/libexec/netdata/plugins.d/alarm-notify.sh test
